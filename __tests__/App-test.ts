it("wrong value", () => {
  const value = 2;
  expect(value).not.toEqual(1);
});
it("good value", () => {
  const value = 2;
  expect(value).toEqual(2);
});
