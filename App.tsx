import React from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableHighlight,
} from "react-native";
import Analytics from "appcenter-analytics";
import { Colors } from "react-native/Libraries/NewAppScreen";

class App extends React.Component {
  __addCartButtonPressed = () => {
    Analytics.trackEvent("Subcribe newsletter", {
      userId: "2443ds5",
      os: "ios",
      screen: "App.tsx",
    });
  };
  render() {
    return (
      <SafeAreaView>
        <TouchableHighlight
          style={styles.addToCartButtonStyle}
          onPress={this.__addCartButtonPressed}
        >
          <Text style={styles.addToCartButtonTextStyle}> Add to Cart </Text>
        </TouchableHighlight>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
  },
  addToCartButtonTextStyle: {
    color: "#fff",
    fontWeight: "bold",
    textAlign: "center",
  },
  addToCartButtonStyle: {
    width: 180,
    height: 45,
    justifyContent: "center",
    borderRadius: 4,
    backgroundColor: "#ff0000",
    alignItems: "center",
  },
});

export default App;
