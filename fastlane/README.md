fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## Android
### android internal
```
fastlane android internal
```
Build app and upload to Playstore (internal track).
### android beta
```
fastlane android beta
```
Build app and upload to Playstore (beta track).
### android develop
```
fastlane android develop
```
Build app and upload to Appcenter.
### android build_prod
```
fastlane android build_prod
```
Build the Android application (for playstore).
### android build_dev
```
fastlane android build_dev
```
Build the Android application (but dont upload).

----

## iOS
### ios beta
```
fastlane ios beta
```
Ship to Testflight.
### ios upload
```
fastlane ios upload
```
Reupload the last build to testflight.

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
